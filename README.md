# Papo livre - manutenção do site

## Como construir o site localmente

Requisitos:

* Ruby
* nanoc 4 (apt-get install nanoc no debian 9+, ou via rubygems)
* inkscape

Pra construir o site basta:

```
$ rake
```

Os arquivos resultantes estarão em `output/`

Pra servir o site localmente você pode usar por exemplo:

```
$ rake server
```

Você pode acessar a sua versão local do site acessando http://localhost:8888/
no seu navegador

## Checklist para lançamento de um novo episódio

* criar diretório template: `rake new`
* obter audios editados
* renomear audios de acordo com o seguinte padrão:
  papolivre-NN-titulo-do-episodio.{ogg,mp3}
* apagar audios de exemplo (`sonar.*`) no diretório do episodio
* colocar audios no diretório do episodio
* ouvir episódio e coletar links
* preencher descrição curta
* preencher corpo do post
* preencher "Edição:"
* preencher "Trilha sonora":
  * link para fonte
  * licença
* fazer capa do episódio (cover.svg)
* fazer capa do áudio (audio-cover.svg)
* fazer um build
* aplicar metadados nos arquivos de audio se necessário (usar easytag):
  * title
  * artist
  * copyright
  * date
  * license
  * website
  * imagem de capa
* checar os áudios (i.e. tocar no totem)
  * mp3
  * ogg
* faker um último build: `rake`
* navegar localmente e checar se está tudo certo
  * rodar `rake server` e acessar http://localhost:8888/
* adicionar arquivos no repositório: `git add`
* dar commit: `git commit`
* atualizar repositório git publico: `git push`
* fazer upload dos audios
* publicar o site

## Checklist para edição de episódio

* fazer o corte inicial, removendo erros e quebrando as faixas nos blocos do
  episódio
* efeitos:
  * redução de ruído
  * remoção de picos
  * compressor
* trilha sonora
  * apenas licenças compatíveis com CC BY-SA
  * tomar nota dos clipes usados para incluir nos créditos
* marcar blocos com rótulos pra virarem capítulos

### Sites onde baixar trilhas sonoras e efeitos

* https://zero-project.gr/    MUSIC
* https://freesound.org/      SFX
* http://www.freesfx.co.uk/   SFX, MUSIC
* https://www.soundjay.com/   SFX, MUSIC
