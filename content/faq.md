# Perguntas Frequentes

## Não consigo encontrar o Papo Livre no meu programa de podcast. Como assinar?

Muitos programas de Podcast utilizam a base de dados do iTunes da Apple para
buscar podcasts. Por várias questões, nós decidimos não nos submeter aos
requisitos pra cadastrar um podcast no iTunes, de forma que se o seu programa
de podcast usa exclusivamente a base de dados do iTunes, você não vai nos
achar mesmo. Procure a opção de "adicionar um podcast manualmente" ou
equivalente no seu aplicativo, e entre uma das seguintes URLs:

* https://papolivre.org/feeds/ogg.xml (áudio `ogg`, em geral arquivos menores)
* https://papolivre.org/feeds/mp3.xml (áudio `mp3`)
