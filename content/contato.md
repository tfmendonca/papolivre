## Contato

### email

Para enviar feedback sobre o podcast, escreva para
[contato@papolivre.org](mailto:contato@papolivre.org).

### IRC

Para conversar sobre o podcast, entre no canal `#papolivre` na Freenode.

### Redes sociais

* Quitter/GNU social: [@slpapolivre](https://quitter.se/slpapolivre)
* Twitter: [@slpapolivre](https://twitter.com/slpapolivre)
