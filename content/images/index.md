# Imagens

## Logotipo

O logotipo do GNU com microfone foi criado por
[Aurélio Heckert](http://softwarelivre.org/aurium)
para o Papo Livre. Ele foi baseado no
[original do projeto GNU](https://www.gnu.org/graphics/heckert_gnu.html) e pode
ser utilizado sob os termos de uma das seguintes licenças:

* [GFDL 1.3](http://www.gnu.org/copyleft/fdl.html)
* [Free Art License](http://directory.fsf.org/wiki/License:FAL1.3)
* [CC-BY-SA 2.0](http://directory.fsf.org/wiki/License:CC_ASA2.0)

Legalmente nós não podemos impedir qualquer uso desta imagem, mas pedimos que
ela seja usada principalmente pra fazer referência ao Papo Livre.

![Logotipo transparente](papolivre.png)
![Logotipo com fundo branco](papolivre-branco.png)

Fonte: [papolivre.svg](papolivre.svg).

## GNU meditante levitando

A imagem do GNU meditante levitando com o microfone foi criada por 
[Aurélio Heckert](http://softwarelivre.org/aurium)
para o Papo Livre. Ela foi baseado na
[original do projeto GNU](https://www.gnu.org/graphics/meditate.html),
e assim como esta, pode ser utilizada sob os termos de umas seguintes licenças:

* [GNU General Public License qersão 3](https://www.gnu.org/copyleft/gpl.html),
  ou alguma versão posterior publicada pela Free Software Foundation.
* [GNU Free Documentation License, Version  1.1](https://www.gnu.org/copyleft/fdl.html)
  ou qualquer versão posterior publicada pela Free Software Foundation. Não
  existem seções invariantes, textos de capa ou textos de contracapa.

![GNU meditante - papo livre](GNU-meditate-papolivre-small.png)

Fonte: [GNU-meditate-papolivre.svg](GNU-meditate-papolivre.svg). Versão *grande*: [GNU-meditate-papolivre.png](GNU-meditate-papolivre.png).
