include Nanoc::Helpers::Blogging
include Nanoc::Helpers::Rendering
include Nanoc::Helpers::LinkTo

SOURCES_ROOT = File.expand_path(File.dirname(__FILE__) + "/..")

class Nanoc::Int::ItemRepWriter
  alias :write_single_orig :write_single
  def write_single(item_rep, snapshot_repo, snapshot_name, raw_path, written_paths)
    if raw_path !~ /\.(ogg|mp3)$/
      return write_single_orig(item_rep, snapshot_repo, snapshot_name, raw_path, written_paths)
    end

    # Create parent directory
    FileUtils.mkdir_p(File.dirname(raw_path))

    # Notify
    Nanoc::Int::NotificationCenter.post(
      :rep_write_started, item_rep, raw_path
    )

    is_created = !File.exists?(raw_path)

    filename = File.basename(raw_path)
    if ENV['NANOC_ENV'] == "production"
      is_modified = is_created || !File.symlink?(raw_path)
      target = "/data/#{filename}"
      raise RuntimeError.new("Missing audio file: #{target}") unless File.exists?(target)
      FileUtils.ln_sf(target, raw_path)
    else
      is_modified = is_created || File.symlink?(raw_path)
      fake = "#{SOURCES_ROOT}/util/sonar#{File.extname(filename)}"
      FileUtils.ln(fake, raw_path, force: true)
    end

    # Notify
    Nanoc::Int::NotificationCenter.post(
      :rep_write_ended, item_rep, true, raw_path, is_created, is_modified
    )
  end
end

class Audio
  def initialize(data, item)
    path = File.dirname(item.identifier).sub(%r{^/}, '')
    filename = data[:filename]
    @filesize = data[:filesize]
    @filename = File.join(path, filename)
    @filepath = File.join('content', @filename)
    @duration_s = item[:duration]
    fail '%s does not specify episode duration!' % item.identifier unless @duration_s
  end

  def url
    '/' + @filename
  end

  def download
    File.basename(@filename)
  end

  def duration
    @duration ||=
      begin
        s = @duration_s.round
        h = s / 3600
        m = (s % 3600) / 60
        s = s - (h*3600) - (m*60)
        '%02d:%02d:%02d' % [h, m, s]
      end
  end

  def length
    @filesize
  end

  def size
    length / 1024**2
  end
end

def audio(episode, format)
  @audio ||= {}
  @audio[format + ':' + episode.identifier] ||= __get_episode_audio__(episode, format)
end

def __get_episode_audio__(episode, format)
  files = episode[:audio].select { |f| f[:filename] =~ /\.#{format}$/ }
  case files.size
  when 0
    fail('Episode %s contains no %s file' % [episode.identifier, format])
  when 1
    :OK
  else
    fail('Episode %s contains more than one %s file' % [episode.identifier, format])
  end
  Audio.new(files[0], episode)
end

def cover_path(episode, variant = nil)
  cover = variant && "cover-#{variant}.png" || "cover.png"
  relative_path_to(File.join(File.dirname(episode.identifier), cover))
end
