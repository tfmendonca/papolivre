require 'pathname'
class AudioDataSource < ::Nanoc::DataSource
  identifier :audio

  def items
    source = Pathname(__FILE__).parent.parent.parent.glob("content/[0-9]*/index.md")
    source.map do |f|
      data = YAML.load_file(f)
      data["audio"].map do |audio|
        filename = audio["filename"]
        new_item('', {}, "/#{f.dirname.basename}/#{filename}")
      end
    end.flatten
  end
end
